@include('welcome')


    <div class="container">
      <div class="card">
          <div class="card-header">
              <h5>General Information:</h5>
          </div>

          <div class="card-body">
              <div class="row">
                  <div class="col-md-12 text-md-center">
                      <img src="/images/{{$user->image}}" class="rounded-circle text-center " style="width: 250px; height: 250px" >
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-12">
                      <h1 class="text-center" style="color: darkslategrey">{{$user->name}}</h1>
                  </div>
              </div>

              <div class="row">
                  <div class="col-md-12">
                      <h5 class="text-center">Email: {{$user->email}}</h5>
                  </div>
              </div>



              <div class="row">
                  <div class="col-md-12  text-md-center">
                      <a class="btn btn-warning" href="{{$user->id}}/edit" style="width: 200px; font-size: 20px;color: black;">Edit</a>
                  </div>
              </div>

                 <div class="row">
                    <div class="col-md-12  text-md-center">
                        <form method="POST" action="/user/{{$user->id}}"enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="DELETE">
                            @csrf
                            <button class="btn btn-danger" type="submit" style="width: 200px;font-size: 20px;color: black">Delete</button>
                        </form>

                  </div>
              </div>

          </div>
      </div>



    </div>