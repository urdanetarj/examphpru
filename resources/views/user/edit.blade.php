@include('welcome')


<div class="container">

    <div class="row">

        <div class="col-md-4"></div>

        <div class="col-md-6">
            <div class="card">
                <div class="card-header text-md-left">
                    <h5>Edit User</h5>
                </div>

                <div class="card-body text-center">
                    <form action="/user/{{$user->id}}" method="POST" enctype="multipart/form-data">
                        <input name="_method" type="hidden" value="PUT">
                        @csrf
                        <div class="form-group ">
                            <div class="row">
                                <div class="col-md-2">
                                    <label class="text-center">Name:</label>
                                </div>
                                <div class="col-md-10">
                                    <input class="form-control text-center" required name="name"  value="{{$user->name}}"  >
                                </div>
                            </div>


                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label>Email:</label>
                                </div>
                                <div class="col-md-10">
                                    <input class="form-control text-center" name="email"  type="email" value="{{$user->email}}">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label>Image:</label>
                                </div>
                                <div class="col-md-6">
                                    <div class="card-img">
                                        <img  src="/images/{{$user->image}}"  style="width: 200px" height="200px" class="rounded-circle">
                                      <!-- <div class="form-group">
                                           <input class="form-control-file" type="file" name="image">
                                       </div>-->
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <button type="submit"  class="btn btn-primary text-md-left">Update Information</button>
                        </div>

                    </form>
                </div>



            </div>
        </div>


    </div>

</div>