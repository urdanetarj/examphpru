@include('welcome')


        <div class="container">

            <div class="row">

                <div class="col-md-4"></div>

                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header text-md-left">
                            <h5>User Register</h5>
                        </div>




                        <div class="card-body text-center">
                            <form action="/user" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label class="text-center">Name:</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input class="form-control text-center" name="name" required placeholder="Enter your name" >
                                        </div>
                                    </div>


                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>Email:</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input class="form-control text-center" type="email" name="email" placeholder="Enter your email" >
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>Image:</label>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="file" class="form-control-file"  name="image" id="Subir Archivo">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-primary text-md-left">Register</button>
                                </div>



                            </form>
                        </div>



                    </div>
                </div>


            </div>

        </div>