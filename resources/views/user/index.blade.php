@include('welcome')


        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <h1 class="text-center">Users List</h1>
                </div>
            </div>

            @if ($message=Session::get('sucess'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif

            @if ($message2=Session::get('update'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message2 }}</strong>
                    </div>
            @endif

            @if ($messsage3=Session::get('deleted'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message3 }}</strong>
                    </div>
                @endif


            @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    Please check the form below for errors
                </div>
            @endif


            <div class="row">
                <table class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>name</th>
                        <th>email</th>
                        <th>image</th>
                        <th>option</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($user as $users)
                        <tr>
                            <th>{{ $users->id }}</th>
                            <td>{{ $users->name }}</td>
                            <td>{{ $users->email }}</td>
                            <td><img src="images/{{$users->image}}" class="rounded-circle" width="100px" height="100px"></td>
                            <td>
                                <a class="btn btn-primary" href="/user/{{$users->id}}" style="width: 70px">Infor</a>

                           @endforeach
                        </tr>
                    </tbody>

                </table>

            </div>
            <div class="row">
                <div class="col-md-12 text-md-center">
                    {{$user->links()}}
                </div>
            </div>
        </div>







