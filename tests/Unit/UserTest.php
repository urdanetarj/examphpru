<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    /**
     * A basic Http de status 200 for the User model
     *
     * @return void
     */
    public function getUsers()
    {

        $response=$this->get('/user');
        $response->assertStatus(200);
    }


    /*
     *
     * This test verifies that can show a User by its 'id' atribute
     */

    public function  showUser()
    {
        $response=$this->get('/user/{id}');
        $response->assertStatus(200);
    }



    /*
     * This test verifies that the email corresponds to an element of the database
     * */
    public function testDataBase()
    {
        $this->assertDatabaseHas('users',
            ['email'=>'urdanetarj1994@gmail.com']);
    }
}
